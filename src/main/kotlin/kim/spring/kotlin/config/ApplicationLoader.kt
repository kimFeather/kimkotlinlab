package kim.spring.kotlin.config

import kim.spring.kotlin.entity.*
import kim.spring.kotlin.repository.*
import kim.spring.kotlin.security.entity.Authority
import kim.spring.kotlin.security.entity.AuthorityName
import kim.spring.kotlin.security.entity.JwtUser
import kim.spring.kotlin.security.repository.AuthorityRepository
import kim.spring.kotlin.security.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Component
//import org.springframework.transaction.annotation.Transactional

import javax.transaction.Transactional

@Component
class ApplicationLoader: ApplicationRunner{
    @Autowired
    lateinit var manufacturerRepository: ManufacturerRepository

    @Autowired
    lateinit var productRepository: ProductRepository

    @Autowired
    lateinit var customerRepository: CustomerRepository

    @Autowired
    lateinit var addressRepository: AddressRepository

    @Autowired
    lateinit var shoppingCartRepository: ShoppingCartRepository

    @Autowired
    lateinit var selectedProduct: SelectedProductRepository

    @Autowired
    lateinit var dataLoader: DataLoader

    @Autowired
    lateinit var authorityRepository: AuthorityRepository
    @Autowired
    lateinit var userRepository: UserRepository
    @Transactional
    fun loadUsernameAndPassword(){
        val auth1 = Authority(name = AuthorityName.ROLE_ADMIN)
        val auth2 = Authority(name = AuthorityName.ROLE_CUSTOMER)
        val auth3 = Authority(name = AuthorityName.ROLE_GENERAL)
        authorityRepository.save(auth1)
        authorityRepository.save(auth2)
        authorityRepository.save(auth3)
        val encoder = BCryptPasswordEncoder()
        val custl = Customer(name = "สมชาติ", email = "a@b.com")
        val cusJwt = JwtUser(
                username = "customer",
                password = encoder.encode("password"),
                email = custl.email,
                enabled = true,
                firstname = custl.name,
                lastname = "unknown"
        )
        customerRepository.save(custl)
        userRepository.save(cusJwt)
        custl.jwtUser = cusJwt
        cusJwt.user = custl
        cusJwt.authorities.add(auth2)
        cusJwt.authorities.add(auth3)
        cusJwt.authorities.add(auth1)
    }

    @Transactional
    override fun run (args: ApplicationArguments?){
        var manu1 = manufacturerRepository.save(Manufacturer("CAMT","0000000"))
        var manu2 = manufacturerRepository.save(Manufacturer("SAMSUNG","555666777888"))
        var manu3 = manufacturerRepository.save(Manufacturer("Apple","44"))
        var manu4 = manufacturerRepository.save(Manufacturer("iPhone","053123456"))

        var product1 = productRepository.save(Product("CAMT","The best College in CMU",
                0.0,1,"http://www.camt.cmu.ac.th/th/images/logo.jpg"))

        var product2 = productRepository.save(Product("Note 9","Other Iphone",
                28001.0,10,"http://dynamic-cdn.eggdigital.com/e56zBiUt1.jpg"))

        var product3 = productRepository.save(Product("Prayuth","The best PM ever",
                1.0,1,"https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Prayut_Chan-o-cha_%28cropped%29_2016.jpg/200px-Prayut_Chan-o-cha_%28cropped%29_2016.jpg"))

        var product4 = productRepository.save(Product("iPhone","It’s a phone",
                28000.0,20,"https://www.jaymartstore.com/Products/iPhone-X-64GB-Space-Grey--1140900010552--4724"))


        var customer1 = customerRepository.save(Customer("Lung","pm@go.th",
                UserStatus.ACTIVE))

        var customer2 = customerRepository.save(Customer("ชัชชาติ","chut@taopoon.com",
                UserStatus.ACTIVE))

        var customer3 = customerRepository.save(Customer("ธนาธร","thanathorn@life.com",
                UserStatus.PENDING))

        var address1 = addressRepository.save(Address("ถนนอนุสาวรีย์ประชาธิปไตย","ดินสอ ",
                "ดุสิต","กรุงเทพ ","10123"))

        var address2 = addressRepository.save(Address("239 มหาวิทยาลัยเชียงใหม่","สุเทพ ",
                "เมือง","เชียงใหม่ ","50200"))

        var address3 = addressRepository.save(Address("ซักที่บนโลก","สุขสันต์ ",
                "เมือง","ขอนแก่น ","12457"))

        var selectedProduct1 = selectedProduct.save(SelectedProduct(4))
        var selectedProduct2 = selectedProduct.save(SelectedProduct(1))
        var selectedProduct3 = selectedProduct.save(SelectedProduct(1))
        var selectedProduct4 = selectedProduct.save(SelectedProduct(2))
        var selectedProduct5 = selectedProduct.save(SelectedProduct(1))

        selectedProduct1.product = product4
        selectedProduct2.product = product3
        selectedProduct3.product = product1
        selectedProduct4.product = product2
        selectedProduct5.product = product3


//
        manu1.products.add(product1)
        product1.manufacturer = manu1

        manu2.products.add(product2)
        product2.manufacturer = manu2

        manu3.products.add(product3)
        product3.manufacturer = manu3

        manu4.products.add(product4)
        product4.manufacturer = manu4

        customer1.defaultAddress = address1
        customer2.defaultAddress = address2
        customer3.defaultAddress = address3
        var shoppingCart1 = shoppingCartRepository.save(ShoppingCart(ShoppingCartStatus.WAIT,customer1))
        var shoppingCart2 = shoppingCartRepository.save(ShoppingCart(ShoppingCartStatus.SENT,customer2))
//
//        shoppingCart1.customer = customer1
//        shoppingCart2.customer = customer2
//
//        shoppingCart1.selectedProducts.add(selectedProduct1)
//        shoppingCart1.selectedProducts.add(selectedProduct2)
        shoppingCart1.selectedProducts= mutableListOf(selectedProduct1,selectedProduct2)
//        shoppingCart2.selectedProducts.add(selectedProduct5)
//        shoppingCart2.selectedProducts.add(selectedProduct3)
//        shoppingCart2.selectedProducts.add(selectedProduct4)
        shoppingCart1.selectedProducts= mutableListOf(selectedProduct5,selectedProduct3,selectedProduct4)

        dataLoader.loadData()
        loadUsernameAndPassword()

    }



}