package kim.spring.kotlin.controller

import kim.spring.kotlin.entity.dto.AddressDto
import kim.spring.kotlin.service.AddressService
import kim.spring.kotlin.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class AddressController {

    @Autowired
    lateinit var addressService: AddressService
//    @GetMapping("/address")
//    fun getAllAddress(): ResponseEntity<Any> {
//        return ResponseEntity.ok( addressService.getAddresss());
//
//    }

    @PostMapping("/address")
    fun getAddresss(@RequestBody address : AddressDto)
            : ResponseEntity<Any> {
        return ResponseEntity.ok(
                MapperUtil.INSTANCE.mapAddress(
                        addressService.save(address)
                )
        )
    }
    @PutMapping("/address")
    fun updateAddress(@RequestBody address: AddressDto)
            :ResponseEntity<Any>{
        if(address.id == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body("id must not be null")
        return ResponseEntity.ok(
                MapperUtil.INSTANCE.mapAddress(
                        addressService.save(address)
                )
        )
    }

    @PutMapping("/address/{addressId}")
    fun updateAddress(@PathVariable("addressId") id: Long?,
                      @RequestBody address: AddressDto)
            :ResponseEntity<Any>{
        address.id = id
        return ResponseEntity.ok(
                MapperUtil.INSTANCE.mapAddress(
                        addressService.save(address)
                )
        )
    }
}

