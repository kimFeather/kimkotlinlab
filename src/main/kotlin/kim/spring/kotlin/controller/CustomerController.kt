package kim.spring.kotlin.controller

import kim.spring.kotlin.entity.UserStatus
import kim.spring.kotlin.entity.dto.CustomerDto
import kim.spring.kotlin.service.CustomerService
import kim.spring.kotlin.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.xml.ws.Response

@RestController

class CustomerController {

    @Autowired
    lateinit var customerService: CustomerService
    @GetMapping("/customer")
    fun getCustomers(): ResponseEntity<Any> {
        val customers = customerService.getCustomers()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapCustomerDto(customers))

    }

    @GetMapping("/customer/query")
    fun getCustomers(@RequestParam("name") name:String): ResponseEntity<Any> {
        var output = MapperUtil.INSTANCE.mapCustomerDto(customerService.getCustomerByName(name))
        output?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @GetMapping("/customer/partial/query")
    fun getCustomerPartial(@RequestParam("name") name : String): ResponseEntity<Any>{
        var output = MapperUtil.INSTANCE.mapCustomerDto(customerService.getCustomerByPartialName(name))
        return ResponseEntity.ok(output)
    }

    @GetMapping("/customer/partial/queryEmail")
    fun getCustomerPartialEmail(@RequestParam("email") email : String): ResponseEntity<Any>{
        var output = MapperUtil.INSTANCE.mapCustomerDto(customerService.getCustomerByPartialEmail(email))
        return ResponseEntity.ok(output)
    }

    @GetMapping("/customer/partial/queryProvince")
    fun getCustomerPartialProvince(@RequestParam("province") province : String): ResponseEntity<Any>{
        var output = MapperUtil.INSTANCE.mapCustomerDto(customerService.getCustomerByPartialProvince(province))
        return ResponseEntity.ok(output)
    }

//    @GetMapping("/customer/status")
//    fun getCustomerByStatus(@RequestParam("status")status: UserStatus): ResponseEntity<Any>{
//        val output = MapperUtil.INSTANCE.mapCustomerDao(
//                customerService.getCustomerByStatus(status)
//        )
//        return ResponseEntity.ok(output)
//    }

    @GetMapping("/customer/status")
    fun getCustomerFromStatus(@RequestParam(name="status") status:String)
    : ResponseEntity<Any>{
        return ResponseEntity.ok(customerService.findByStatus(status))
    }

    @GetMapping("/customer/product")
    fun getCustomerFromBoughtProduct(@RequestParam (name ="name") name:String)
    :ResponseEntity<Any>{
        return ResponseEntity.ok(customerService.findByBoughtProduct(name))
    }

//    @PostMapping("/customer")
//    fun addProduct(@RequestBody customerDto:CustomerDto)
//            :ResponseEntity<Any>{
//        val output= customerService.save(MapperUtil.INSTANCE.mapCustomerDto(customerDto))
//        val outputDto = MapperUtil.INSTANCE.mapCustomerDto(output)
//        outputDto?.let{ return ResponseEntity.ok(it) }
//        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
//    }

    @PostMapping("/customer/address/{addressId}")
    fun addProduct(@RequestBody customerDto: CustomerDto,
                   @PathVariable addressId:Long)
            :ResponseEntity<Any>{
        val output = customerService.save(addressId,MapperUtil.INSTANCE.mapCustomerDto(customerDto))
        val outputDto = MapperUtil.INSTANCE.mapCustomerDto(output)
        outputDto?.let{ return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

//    @PostMapping("/customer/manufacturer/{cusId}")
//    fun addProduct(@RequestBody customerDto: CustomerDto,
//                   @PathVariable cusId:Long)
//            :ResponseEntity<Any>{
//        val output = customerService.save(cusId,MapperUtil.INSTANCE.mapCustomerDto(customerDto))
//        val outputDto = MapperUtil.INSTANCE.mapCustomerDto(output)
//        outputDto?.let{ return ResponseEntity.ok(it) }
//        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
//    }

}