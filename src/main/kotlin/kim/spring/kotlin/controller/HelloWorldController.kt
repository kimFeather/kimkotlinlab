package kim.spring.kotlin.controller

import kim.spring.kotlin.entity.Person
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class HelloWorldController{
    @GetMapping("/helloWorld")
    fun getHelloWorld():String{
        return "HelloWorld"
    }

    @GetMapping("/camt")
    fun getCamt():String{
        return "CAMT"
    }

    @GetMapping("/myPerson")
    fun getMyPerson():ResponseEntity<Any> {
        val person = Person("kim","naja",15)
        return ResponseEntity.ok(person)
    }

    @GetMapping("/person")
    fun getPerson(): ResponseEntity<Any> {
        val person01 = Person("somchai","somrak",15)
        val person02 = Person("Prayut","Chan",15)
        val person03 = Person("kim","watchman",15)
        val persons = listOf<Person>(person01,person02,person03)
        return ResponseEntity.ok(persons)
    }

    @GetMapping("/params")
    fun getParams(@RequestParam("name")name: String, @RequestParam("surname") surname:String)
            : ResponseEntity<Any> {
        return ResponseEntity.ok("$name $surname")
    }

    @GetMapping("/params/{name}/{surname}/{age}")
    fun getPathParams(@PathVariable("name")name: String,
                      @PathVariable("surname") surname:String,
                      @PathVariable("age")age: Int)
            : ResponseEntity<Any> {
        val person = Person(name,surname,age)
        return ResponseEntity.ok(person)
    }

    @PostMapping("/echo")
    fun echo (@RequestBody person: Person) :ResponseEntity<Any>
    {
        return ResponseEntity.ok(person)
    }}

