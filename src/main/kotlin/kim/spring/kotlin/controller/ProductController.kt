package kim.spring.kotlin.controller


import kim.spring.kotlin.entity.Product
import kim.spring.kotlin.entity.dto.ProductDto
import kim.spring.kotlin.service.ProductService
import kim.spring.kotlin.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController

class ProductController {

    @Autowired
    lateinit var productService: ProductService

    @GetMapping("/product")
    fun getProducts(): ResponseEntity<Any> {
        val products = productService.getProducts()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapProductDto(products))
    }
    @GetMapping("/product/query")
    fun getProducts(@RequestParam("name") name:String): ResponseEntity<Any> {
        var output = MapperUtil.INSTANCE.mapProductDto(productService.getProductByName(name))
        output?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @GetMapping("/product/partialQuery")
    fun getProductsPartial(@RequestParam("name") name : String,
                           @RequestParam(value = "desc",required = false) desc:String?): ResponseEntity<Any>{
        val output:List<Any> = MapperUtil.INSTANCE.mapProductDto(
                productService.getProductByPartialNameAndDesc(name,name))
        return ResponseEntity.ok(output)
    }

    @GetMapping("/product/{manuName}")
    fun getProductByManuName(@PathVariable("manuName") name:String) : ResponseEntity<Any> {
        val output:List<Any> = MapperUtil.INSTANCE.mapProductDto(
                productService.getProductByManuName(name))
        return ResponseEntity.ok(output)
    }

    @PostMapping("/product")
    fun addProduct(@RequestBody productDto:ProductDto)
    :ResponseEntity<Any>{
        val output= productService.save(MapperUtil.INSTANCE.mapProductDto(productDto))
        val outputDto = MapperUtil.INSTANCE.mapProductDto(output)
        outputDto?.let{ return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @PostMapping("/product/manufacturer/{manuId}")
    fun addProduct(@RequestBody productDto: ProductDto,
                   @PathVariable manuId:Long)
    :ResponseEntity<Any>{
        val output = productService.save(manuId,MapperUtil.INSTANCE.mapProductDto(productDto))
        val outputDto = MapperUtil.INSTANCE.mapProductDto(output)
        outputDto?.let{ return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @DeleteMapping("/product/{id}")
    fun deleteProduct(@PathVariable("id") id:Long):ResponseEntity<Any>{
        val product = productService.remove(id)
        val outputProduct = MapperUtil.INSTANCE.mapProductDto(product)
        outputProduct?.let {return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("the product id is not found")
    }

//    @GetMapping("/product/name")
//    fun getProductWithPage(@RequestParam("name")name: String,
//                           @RequestParam("page")page:Int,
//                           @RequestParam("pageSize")pageSize:Int):ResponseEntity<Any>{
//        val output = productService.getProductsWithPage(name,page,pageSize)
//        return ResponseEntity.ok(PageProductDto(totalPages = output.totalPages,
//                totalElements = output.totalElements,
//                products = MapperUtil.INSTANCE.mapProductDto(output.content)))
//    }

//    @GetMapping("/product/partial/query")
//   fun getProductsPartial(@RequestParam("name") name : String): ResponseEntity<Any>{
//        var output = MapperUtil.INSTANCE.mapProductDto(productService.getProductByPartialName(name))
//        return ResponseEntity.ok(output)
//    }

//    @GetMapping("/product/partialQuery")
//    fun getProductsPartial(@RequestParam("name") name : String,
//                           @RequestParam(value = "desc",required = false) desc:String?): ResponseEntity<Any>{
//        val output:List<Any>
//        if (desc == null){
//            output = MapperUtil.INSTANCE.mapProductDto(productService.getProductByPartialName(name))
//        } else {
//            output = MapperUtil.INSTANCE.mapProductDto(productService.getProductByPartialNameAndDesc(name,desc))
//        }
//
////        var output = MapperUtil.INSTANCE.mapProductDto(productService.getProductByPartialName(name))
//        return ResponseEntity.ok(output)
//    }





//
//    @Autowired
//    lateinit var productService: ProductService
//    @GetMapping("/product")
//    fun getAllProduct(): ResponseEntity<Any> {
//        val products = productService.getProducts()
//        val productDtos = mutableListOf<ProductDto>()
//        for(product in products){
//            productDtos.add(ProductDto(
//                    product.name,
//                    product.description,
//                    product.price,
//                    product.amountInStock,
//                    product.imageUrl))
//        }
//        return ResponseEntity.ok(products);
//

}