package kim.spring.kotlin.controller

import kim.spring.kotlin.entity.dto.PageSelectedProductDto
import kim.spring.kotlin.service.SelectedProductService
import kim.spring.kotlin.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
class SelectedProductController {
    @Autowired
    lateinit var selectedProductService: SelectedProductService

    @GetMapping("/selectedProduct/productName")
    fun getSelectedProductByProductName(@RequestParam ("query") query: String,
                                        @RequestParam ("page") page: Int,
                                        @RequestParam ("pageSize") pageSize: Int)
    :ResponseEntity<Any>{
        val output  = selectedProductService.findByProductName(query,page,pageSize)
        return ResponseEntity.ok(
                PageSelectedProductDto(
                        totalElements = output.totalElements,
                        totalPages = output.totalPages,
                        selectedProduct = MapperUtil.INSTANCE.mapSelectedProduct(output.content)
                )
        )
    }
}