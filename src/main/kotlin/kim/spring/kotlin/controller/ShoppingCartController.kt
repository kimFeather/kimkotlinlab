package kim.spring.kotlin.controller

import kim.spring.kotlin.entity.dto.PageShoppingCustomerDto
import kim.spring.kotlin.service.ShoppingCartService
import kim.spring.kotlin.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.lang.System.out

@RestController
class ShoppingCartController {

    @Autowired
    lateinit var shoppingCartService: ShoppingCartService

    @GetMapping("/shoppingcart")
    fun getShoppingCart(): ResponseEntity<Any> {
        val shoppingCarts = shoppingCartService.getShoppingCarts()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapShoppingCartDao(shoppingCarts))


    }

    @GetMapping("/allShoppingCart")
    fun getAllShoppingCart(
            @RequestParam("page") page:Int,
            @RequestParam("pageSize") pageSize:Int) : ResponseEntity<Any> {
        val output = shoppingCartService.getShoppingCart(page,pageSize)
        return ResponseEntity.ok(PageShoppingCustomerDto(
                totalPage = output.totalPages,
                totalElement = output.totalElements,
                shoppingList = MapperUtil.INSTANCE.mapShoppingCartCustomerDao(output.content)

        ))
    }

    @GetMapping("/shoppingCart/productName")
    fun getShoppingCart(
            @RequestParam("query") query: String,
            @RequestParam("page") page: Int,
            @RequestParam("pageSize") pageSize: Int)
    :ResponseEntity<Any>{
        val output = shoppingCartService.findByProductName(query,page,pageSize)
        return ResponseEntity.ok(PageShoppingCustomerDto(
                totalPage = output.totalPages,
                totalElement = output.totalElements,
                shoppingList = MapperUtil.INSTANCE.mapShoppingCartCustomerDao(output.content)
        ))
    }



}