package kim.spring.kotlin.dao

import kim.spring.kotlin.entity.Address

interface  AddressDao {
//    fun getAddresss():List<Address>
    fun save(address: Address): Address

    fun findById(addressId: Long): Address?
}