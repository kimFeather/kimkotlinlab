package kim.spring.kotlin.dao

import kim.spring.kotlin.entity.Address
import kim.spring.kotlin.repository.AddressRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository


@Repository
class  AddressDaoImpl : AddressDao {
    override fun findById(addressId: Long): Address? {
        return AddressRepository.findById(addressId).orElse(null)
    }

    @Autowired
    lateinit var AddressRepository: AddressRepository
    override fun save(address: Address): Address {
        return AddressRepository.save(address)
    }



}