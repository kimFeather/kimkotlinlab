package kim.spring.kotlin.dao

import kim.spring.kotlin.entity.Customer
import kim.spring.kotlin.entity.UserStatus

interface CustomerDao{
    fun getCustomers():List<Customer>
    fun getCustomerByPartialName(name: String): List<Customer>
    fun getCustomerByName(name: String): Customer?
    fun getCustomerByPartialEmail(email: String): List<Customer>
    fun getCustomerByPartialProvince(province: String): List<Customer>
    fun getCustomerByStatus(status: UserStatus): List<Customer>
    fun findByStatus(status: String): List<Customer>
    fun save(customer:Customer ): Customer
}
