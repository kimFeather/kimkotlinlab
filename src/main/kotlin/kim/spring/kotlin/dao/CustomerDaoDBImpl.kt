package kim.spring.kotlin.dao

import kim.spring.kotlin.entity.Customer
import kim.spring.kotlin.entity.UserStatus
import kim.spring.kotlin.repository.CustomerRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.data.jpa.domain.AbstractPersistable_.id
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class CustomerDaoDBImpl: CustomerDao{
    override fun save(customer: Customer): Customer {
        return  customerRepository.save(customer)
    }


    override fun findByStatus(status: String): List<Customer> {
       try{
           return customerRepository.findByUserStatus(UserStatus.valueOf(status.toUpperCase()))
       }catch (e:IllegalArgumentException){
           return emptyList()
       }
    }


    override fun getCustomerByStatus(status: UserStatus): List<Customer> {
        return customerRepository.findByUserStatus(status)
    }

    override fun getCustomerByPartialProvince(province: String): List<Customer> {
        return customerRepository.findByDefaultAddress_ProvinceContainingIgnoreCase(province)
    }

    override fun getCustomerByPartialEmail(email: String): List<Customer> {
        return customerRepository.findByEmailEndingWith(email)
    }

    override fun getCustomerByName(name: String): Customer? {
        return customerRepository.findByName(name)

    }

    override fun getCustomerByPartialName(name: String): List<Customer> {

        return customerRepository.findByNameEndingWith(name)
    }

    @Autowired
    lateinit var customerRepository: CustomerRepository

    override fun getCustomers(): List<Customer> {
        return customerRepository.findAll().filterIsInstance(Customer::class.java)
    }
}