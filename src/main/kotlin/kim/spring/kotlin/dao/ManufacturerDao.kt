package kim.spring.kotlin.dao

import kim.spring.kotlin.entity.Manufacturer
import kim.spring.kotlin.repository.ManufacturerRepository

interface ManufacturerDao{
    fun getManufacturers():List<Manufacturer>


    fun save(manufacturer: Manufacturer): Manufacturer
    fun findById(manuId: Long): Manufacturer?
}

