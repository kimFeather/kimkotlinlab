package kim.spring.kotlin.dao

import kim.spring.kotlin.entity.SelectedProduct
import org.springframework.data.domain.Page

interface SelectedProductDao{
    fun  findByProductName(name: String , page:Int,pageSize: Int): Page<SelectedProduct>
}