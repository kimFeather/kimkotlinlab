package kim.spring.kotlin.dao

import kim.spring.kotlin.entity.ShoppingCart
import org.springframework.data.domain.Page

interface ShoppingCartDao{

    fun getShoppingCarts():List<ShoppingCart>
    fun getShoppingCart(page: Int, pageSize: Int): Page<ShoppingCart>
    fun findByProductName(query: String, page: Int, pageSize: Int): Page<ShoppingCart>
    fun findByProductName(query: String): List<ShoppingCart>
}
