package kim.spring.kotlin.dao

import kim.spring.kotlin.entity.ShoppingCart
import kim.spring.kotlin.repository.ShoppingCartRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class ShoppingCartDaoDBImpl: ShoppingCartDao{

    override fun findByProductName(query:String):List<ShoppingCart>{
        return shoppingCartRepository.findBySelectedProducts_Product_NameContainingIgnoreCase(query)
    }

    override fun findByProductName(query: String, page: Int, pageSize: Int): Page<ShoppingCart> {
        return shoppingCartRepository
                .findBySelectedProducts_Product_NameContainingIgnoreCase(
                        query,
                        PageRequest.of(page,pageSize)
                )
    }

    override fun getShoppingCart(page: Int, pageSize: Int): Page<ShoppingCart> {
        return shoppingCartRepository.findAll(PageRequest.of(page,pageSize))
    }

    @Autowired
    lateinit var shoppingCartRepository: ShoppingCartRepository

    override fun getShoppingCarts(): List<ShoppingCart> {
        return shoppingCartRepository.findAll().filterIsInstance(ShoppingCart::class.java)
    }
}