package kim.spring.kotlin.dao

import kim.spring.kotlin.entity.ShoppingCart
import org.springframework.context.annotation.Profile
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Repository
import kim.spring.kotlin.repository.ShoppingCartRepository

@Profile("mem")
@Repository
class ShoppingCartDaoImpl:ShoppingCartDao{
    override fun findByProductName(query: String): List<ShoppingCart> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findByProductName(query: String, page: Int, pageSize: Int): Page<ShoppingCart> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
//    override fun findByProductName(query: String, page: Int, pageSize: Int): Page<ShoppingCart> {
//       return shoppingCartRepository
//               .findByProducts_Product_NameContainingIgnoreCase(
//                       query,
//                       PageRequest.of(page,pageSize)
//               )
//    }

    override fun getShoppingCart(page: Int, pageSize: Int): Page<ShoppingCart> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getShoppingCarts(): List<ShoppingCart> {
        TODO()
//        return mutableListOf(ShoppingCart(ShoppingCartStatus.WAIT),
//                ShoppingCart(ShoppingCartStatus.SENT))

    }

//    override fun getShoppingCart(page: Int, pageSize: Int): Page<ShoppingCart> {
//        return shoppingCartRepository.findAll(PageRequest.of(page,pageSize))
//    }
}