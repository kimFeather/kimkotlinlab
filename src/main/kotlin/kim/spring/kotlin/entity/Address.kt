package kim.spring.kotlin.entity

import javax.persistence.*

@Entity
data class Address (var homeAddress: String? = null,
                    var subdistrict: String? = null,
                    var district: String? = null,
                    var province: String? =null,
                    var postCode: String? =null){
    @Id
    @GeneratedValue
    var id:Long? = null




}


