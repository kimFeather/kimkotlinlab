package kim.spring.kotlin.entity

import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping

data class Person(var name:String, var surname:String , var age:Int)

//@GetMapping("/person")
//fun getPerson():ResponseEntity<Any>{
//    val person01 = Person("somchai","somrak",15)
//    val person02 = Person("Prayut","Chan",15)
//    val person03 = Person("kim","watchman",15)
//    val persons = listOf<Person>(person01,person02,person03)
//    return ResponseEntity.ok(persons)
//}
