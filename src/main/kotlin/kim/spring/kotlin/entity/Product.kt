package kim.spring.kotlin.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.ManyToOne


@Entity
data class Product(var name:String? =null,
                   var description:String? = null,
                   var price:Double? = null,
                   var amountInStock:Int? = null,
                   var imageUrl: String?=null,
                   var isDeleted: Boolean = false){

    @Id
    @GeneratedValue
    var id:Long? = null
    @ManyToOne
    var manufacturer: Manufacturer? =null
    constructor( name:String,
                 description:String,
                 price:Double,
                 amountInStock:Int,
                 imageUrl: String?,
                 manufacturer: Manufacturer
                 ):
            this(name,description,price,amountInStock,imageUrl){
        this.manufacturer = manufacturer
    }
}