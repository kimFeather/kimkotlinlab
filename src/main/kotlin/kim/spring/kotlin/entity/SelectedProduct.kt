package kim.spring.kotlin.entity

import javax.persistence.*

@Entity
data class SelectedProduct(
                           var quantity: Int){
    @Id
    @GeneratedValue
    var id:Long? = null

    @OneToOne
    var product:Product? =null




}