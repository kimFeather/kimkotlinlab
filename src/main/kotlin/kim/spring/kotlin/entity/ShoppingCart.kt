package kim.spring.kotlin.entity

import javax.persistence.*

@Entity
data class ShoppingCart(var status:ShoppingCartStatus? = ShoppingCartStatus.WAIT, @OneToOne
var customer: Customer){

    @Id
    @GeneratedValue
    var id:Long? = null

    @OneToMany
    var selectedProducts =  mutableListOf<SelectedProduct>()

    @OneToOne
     var shoppingAddress : Address? = null



}