package kim.spring.kotlin.entity


enum class ShoppingCartStatus{
    WAIT,
    CONFIRM,
    PAID,
    SENT,
    RECEIVED
}