package kim.spring.kotlin.entity


enum class UserStatus {
    PENDING, ACTIVE, NOTACTIVE, DELETED
}