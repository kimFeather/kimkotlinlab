package kim.spring.kotlin.entity.dto

import kim.spring.kotlin.entity.Address
import kim.spring.kotlin.entity.UserStatus

data class CustomerDto(var name:String? = null,
                       var email:String? = null,
                       var userStatus:  UserStatus? = UserStatus.PENDING,
                       var defaultAddress: Address? = null,
                       var shippingAddress: List<Address>? = mutableListOf<Address>() ,
                       var billingAddress: Address? =null,
                       var id:Long? =null)