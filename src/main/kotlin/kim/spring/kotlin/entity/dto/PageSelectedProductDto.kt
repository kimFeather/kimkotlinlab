package kim.spring.kotlin.entity.dto

data class PageSelectedProductDto (var totalPages: Int? = null,
                                   var totalElements:Long?= null,
                                   var selectedProduct: List<SelectedProductDto> = mutableListOf())
//                                   var products: List<ProductDto> = mutableListOf())