package kim.spring.kotlin.entity.dto

import kim.spring.kotlin.entity.Product

data class SelectedProductDto(
        var quantity:Int? =null,
        var product: ProductDto? = null

)