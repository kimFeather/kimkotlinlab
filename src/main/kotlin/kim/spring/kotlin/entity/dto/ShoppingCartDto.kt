package kim.spring.kotlin.entity.dto

import kim.spring.kotlin.entity.Address
import kim.spring.kotlin.entity.Customer
import kim.spring.kotlin.entity.SelectedProduct
import kim.spring.kotlin.entity.ShoppingCartStatus

data class ShoppingCartDto(
        var status: ShoppingCartStatus? = null,
        var selectedProducts: List<SelectedProductDto>? = mutableListOf<SelectedProductDto>(),
        var shoppingAddress : Address? = null,
        var  customer: Customer? = null
        )