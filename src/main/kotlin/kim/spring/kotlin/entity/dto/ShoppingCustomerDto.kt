package kim.spring.kotlin.entity.dto

import kim.spring.kotlin.entity.Address

data class ShoppingCustomerDto (var customer: String? = null,
                                var address: Address? =null,
                                var products: List<DisplayProduct>? =null)

data class DisplayProduct (var name : String? =null,
                           var description : String? =null,
                           var quantity: Int? = null)

data class PageShoppingCustomerDto (var totalPage: Int,
                                    var totalElement: Long,
                                    var shoppingList: List<ShoppingCustomerDto> )

