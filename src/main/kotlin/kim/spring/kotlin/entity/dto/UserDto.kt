package kim.spring.kotlin.entity.dto

import kim.spring.kotlin.security.entity.Authority

class UserDto ( var name : String? =null,
                var email : String? =null,
                var username: String? =null,
                var authorities: List<AuthorityDto> = mutableListOf())