package kim.spring.kotlin.repository


import kim.spring.kotlin.entity.Address
import org.springframework.data.repository.CrudRepository

interface AddressRepository: CrudRepository<Address, Long>{

}