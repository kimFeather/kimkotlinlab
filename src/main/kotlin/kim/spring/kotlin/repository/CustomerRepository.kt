package kim.spring.kotlin.repository

import kim.spring.kotlin.entity.Customer
import kim.spring.kotlin.entity.UserStatus
import org.springframework.data.repository.CrudRepository


interface CustomerRepository: CrudRepository<Customer, Long> {
     fun findByName(name : String):Customer?
     fun findByNameEndingWith(name: String) :List<Customer>
     fun findByEmailEndingWith(email: String): List<Customer>
     fun findByDefaultAddress_ProvinceContainingIgnoreCase(province : String): List<Customer>
     fun findByUserStatus(status: UserStatus): List<Customer>



}