package kim.spring.kotlin.repository

import kim.spring.kotlin.entity.Manufacturer
import org.springframework.data.repository.CrudRepository

interface ManufacturerRepository: CrudRepository<Manufacturer,Long>{
    fun findByName(name:String): Manufacturer
}