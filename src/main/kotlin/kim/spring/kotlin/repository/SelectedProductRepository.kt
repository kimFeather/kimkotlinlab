package kim.spring.kotlin.repository

import kim.spring.kotlin.entity.SelectedProduct
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.CrudRepository


interface SelectedProductRepository: CrudRepository<SelectedProduct, Long>{
    fun findByProduct_Name(name: String , pageable: Pageable): Page<SelectedProduct>
}