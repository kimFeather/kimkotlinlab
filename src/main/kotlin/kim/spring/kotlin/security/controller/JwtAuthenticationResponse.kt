package kim.spring.kotlin.security.controller

data class JwtAuthenticationResponse(
        var token: String? = null
)