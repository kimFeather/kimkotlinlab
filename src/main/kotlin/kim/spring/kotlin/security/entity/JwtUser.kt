package kim.spring.kotlin.security.entity

import kim.spring.kotlin.entity.User
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import java.util.*
import javax.persistence.*

@Entity
data class JwtUser(
        private var username: String? = null,
        var firstname: String? = null,
        var lastname: String? = null,
        private var password: String? = null,
        var email: String? = null,
        //private var authorities: MutableCollection<GrantedAuthority> = mutableListOf(),
        var enabled: Boolean = false,
        var lastPasswordResetDate: Date? = null) : UserDetails {
    @Id
    @GeneratedValue
    var id: Long? = null
    @ManyToMany
    var authorities = mutableListOf<Authority>()
    @OneToOne(mappedBy = "jwtUser")
    var user: User? = null
    @Transient
    var grantedAuthorities: MutableCollection<out GrantedAuthority> = mutableListOf()
    override fun isEnabled(): Boolean {
        return enabled
    }

    override fun getUsername(): String? {
        return username
    }

    override fun isCredentialsNonExpired(): Boolean {
        return true
    }

    override fun getPassword(): String? {
        return password
    }

    override fun isAccountNonExpired(): Boolean {
        return true
    }

    override fun isAccountNonLocked(): Boolean {
        return true
    }

    override fun getAuthorities(): MutableCollection<out GrantedAuthority> {
        if (grantedAuthorities.size >0 ){
            return grantedAuthorities
        }else{
            grantedAuthorities = authorities
                    .map { authority -> SimpleGrantedAuthority(authority.name?.name) }
                    .toMutableList()
            return grantedAuthorities
        }

    }
}
