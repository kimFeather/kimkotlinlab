package kim.spring.kotlin.security.repository

import kim.spring.kotlin.security.entity.Authority
import kim.spring.kotlin.security.entity.AuthorityName
import org.springframework.data.repository.CrudRepository

interface AuthorityRepository: CrudRepository<Authority, Long> {
    fun findByName(input: AuthorityName): Authority
}