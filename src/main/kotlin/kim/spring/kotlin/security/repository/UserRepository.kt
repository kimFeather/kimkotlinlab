package kim.spring.kotlin.security.repository

import kim.spring.kotlin.security.entity.JwtUser
import org.springframework.data.repository.CrudRepository


interface UserRepository: CrudRepository<JwtUser, Long> {
    fun findByUsername(username: String): JwtUser
}