package kim.spring.kotlin.service

import kim.spring.kotlin.entity.Address
import kim.spring.kotlin.entity.dto.AddressDto

interface AddressService {
//    fun getAddresss(): List<Address>
    fun save(address: AddressDto): Address
}