package kim.spring.kotlin.service

import kim.spring.kotlin.dao.AddressDao
import kim.spring.kotlin.entity.Address
import kim.spring.kotlin.entity.dto.AddressDto
import kim.spring.kotlin.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class AddressServiceImpl : AddressService {

    @Autowired
    lateinit var addressDao: AddressDao
//    override fun getAddresss(): List<Address> {
//        return addressDao.getAddresss()
//    }

    override fun save(address: AddressDto): Address {
        val address = MapperUtil.INSTANCE.mapAddress(address)
        return addressDao.save(address)
    }


}