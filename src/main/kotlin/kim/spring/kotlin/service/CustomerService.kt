package kim.spring.kotlin.service

import kim.spring.kotlin.entity.Customer
import kim.spring.kotlin.entity.UserStatus

interface CustomerService {
    fun getCustomers() : List<Customer>
    fun getCustomerByPartialName(name: String): List<Customer>
    fun getCustomerByName(name: String): Customer?
//    fun getCustomerByEmail(email: String): List<Customer>
    fun getCustomerByPartialEmail(email: String): List<Customer>
    fun getCustomerByPartialProvince(province: String): List<Customer>
    fun getCustomerByStatus(status: UserStatus): List<Customer>
    fun findByStatus(status: String): List<Customer>
    fun findByBoughtProduct(name: String): List<Customer>
    fun save(addressId: Long,customer: Customer): Customer


}