package kim.spring.kotlin.service

import kim.spring.kotlin.dao.AddressDao
import kim.spring.kotlin.dao.CustomerDao
import kim.spring.kotlin.dao.ShoppingCartDao
import kim.spring.kotlin.entity.Customer
import kim.spring.kotlin.entity.UserStatus
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import javax.xml.ws.Response

@Service
class CustomerServiceImpl : CustomerService {


    @Autowired
    lateinit var addressDao: AddressDao

    override fun save(addressId: Long, customer: Customer): Customer {
        val address = addressDao.findById(addressId)
        val customer = customerDao.save(customer)
        address?.let { addressDao.save(it) }
        customer.defaultAddress = address
        return customer
    }

//    @Autowired
//    lateinit var addressDao: AddressDao
//    override fun save(addressId: Long, customer: Customer): Customer {
//        val address = customer.defaultAddress?.let{ addressDao.save(it)}
//        val customer = customerDao.save(customer)
//        customer.defaultAddress = address
//        return customer
//    }


//    override fun save(cusId: Long, mapCustomerDto: Customer): Customer {
//        val manufacturer = manufacturerDao.findById(cusId)
//        val customer = customerDao.save(customer)
//        customer.manufacturer = manufacturer
//        manufacturer?.customers?.add(customer)
//        return customer
//    }


    @Autowired
    lateinit var shoppingCartDao: ShoppingCartDao

    override fun findByBoughtProduct(name: String): List<Customer> {
        return shoppingCartDao.findByProductName(name)
                .map { shoppingCart -> shoppingCart.customer }
                .toSet().toList()
    }

    override fun findByStatus(status: String): List<Customer> {
        return customerDao.findByStatus(status)
    }

    override fun getCustomerByStatus(status: UserStatus): List<Customer> {
        return customerDao.getCustomerByStatus(status)
    }

    override fun getCustomerByPartialProvince(province: String): List<Customer> {
        return customerDao.getCustomerByPartialProvince(province)
    }

    override fun getCustomerByPartialEmail(email: String): List<Customer> {
        return customerDao.getCustomerByPartialEmail(email)
    }
//    override fun getCustomerByEmail(email: String): List<Customer> {
//        return customerDao.getCustomerByEmail
//    }

    override fun getCustomerByName(name: String): Customer? {
        return customerDao.getCustomerByName(name)
    }

    override fun getCustomerByPartialName(name: String): List<Customer> {
        return customerDao.getCustomerByPartialName(name)

    }

    @Autowired
    lateinit var customerDao: CustomerDao

    override fun getCustomers(): List<Customer> {
        return customerDao.getCustomers()
    }
}