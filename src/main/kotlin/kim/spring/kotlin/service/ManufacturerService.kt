package kim.spring.kotlin.service

import kim.spring.kotlin.entity.Manufacturer
import kim.spring.kotlin.entity.dto.ManufacturerDto
import kim.spring.kotlin.util.MapperUtil

interface ManufacturerService {
    fun getManufacturers() : List<Manufacturer>
    fun save(manu: ManufacturerDto): Manufacturer


//    fun save(manu: ManufacturerDto): Manufacturer
}