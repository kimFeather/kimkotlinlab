package kim.spring.kotlin.service

import kim.spring.kotlin.dao.ManufacturerDao
import kim.spring.kotlin.entity.Manufacturer
import kim.spring.kotlin.entity.dto.ManufacturerDto
import kim.spring.kotlin.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class ManufacturerServiceImpl: ManufacturerService{


    @Autowired
    lateinit var manufacturerDao: ManufacturerDao
    override fun getManufacturers(): List<Manufacturer> {
        return manufacturerDao.getManufacturers()
    }

    override fun save (manu: ManufacturerDto): Manufacturer{
        val manufacturer = MapperUtil.INSTANCE.mapManufacturer(manu)
        return manufacturerDao.save(manufacturer)
    }
}