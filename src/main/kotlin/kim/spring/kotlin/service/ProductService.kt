package kim.spring.kotlin.service

import kim.spring.kotlin.entity.Product
import org.springframework.data.domain.Page

interface ProductService {
    fun getProducts() : List<Product>
    fun getProductByName(name:String): Product?
    abstract fun getProductByPartialName(name: String): List<Product>
    abstract fun getProductByPartialNameAndDesc(name: String, desc: String): List<Product>
    fun getProductByManuName(name: String): List<Product>
    fun getProductsWithPage(name: String, page: Int, pageSize: Int): Page<Product>
    fun save(mapProductDto: Product): Product
    fun save(manuId:Long , product: Product): Product
    fun remove(id: Long): Product?

}