package kim.spring.kotlin.service

import kim.spring.kotlin.entity.SelectedProduct
import org.springframework.data.domain.Page

interface SelectedProductService {
    fun findByProductName(query: String, page: Int, pageSize: Int): Page<SelectedProduct>

}