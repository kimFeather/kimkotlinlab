package kim.spring.kotlin.service


import kim.spring.kotlin.dao.SelectedProductDao
import kim.spring.kotlin.entity.SelectedProduct
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service

@Service
class SelectedProductServiceImpl : SelectedProductService{
    override fun findByProductName(query: String, page: Int, pageSize: Int): Page<SelectedProduct> {
       return selectedProductDao.findByProductName(query,page,pageSize)
    }

    @Autowired
    lateinit var selectedProductDao: SelectedProductDao
}