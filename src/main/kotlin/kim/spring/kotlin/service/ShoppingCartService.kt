package kim.spring.kotlin.service

import kim.spring.kotlin.entity.ShoppingCart
import org.springframework.data.domain.Page

interface ShoppingCartService {
    fun getShoppingCarts() : List<ShoppingCart>
    fun getShoppingCart(page: Int, pageSize: Int): Page<ShoppingCart>
    fun findByProductName(query: String, page: Int, pageSize: Int): Page<ShoppingCart>
//    fun getShoppingCart(page: Int, pageSize: Int): Page<ShoppingCart> {
//        return shoppingCartDao.getShoppingCart(page,pageSize)
//    }
}