package kim.spring.kotlin.service

import kim.spring.kotlin.dao.ShoppingCartDao
import kim.spring.kotlin.entity.ShoppingCart
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service

@Service
class ShoppingCartServiceImpl: ShoppingCartService{
    override fun findByProductName(query: String, page: Int, pageSize: Int): Page<ShoppingCart> {
        return shoppingcartDao.findByProductName(query,page,pageSize)
    }

    @Autowired
    lateinit var shoppingcartDao: ShoppingCartDao
    override fun getShoppingCarts(): List<ShoppingCart> {
        return shoppingcartDao.getShoppingCarts()
    }

    override fun getShoppingCart(page: Int, pageSize: Int): Page<ShoppingCart> {
        return shoppingcartDao.getShoppingCart(page,pageSize)
    }

}