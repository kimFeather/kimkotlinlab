package kim.spring.kotlin.util

import kim.spring.kotlin.entity.*
import kim.spring.kotlin.entity.dto.*
import kim.spring.kotlin.security.entity.Authority
import org.mapstruct.InheritInverseConfiguration
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings
import org.mapstruct.factory.Mappers


@Mapper(componentModel = "spring")
interface MapperUtil{
    companion object {
        val INSTANCE = Mappers.getMapper(MapperUtil::class.java)
    }

    @Mappings(
        Mapping(source = "manufacturer",target = "manu")
    )

    fun mapProductDto(product: Product?):ProductDto?
    fun mapProductDto (products: List<Product>):List<ProductDto>

    fun mapManufacturer(manu: Manufacturer): ManufacturerDto
    fun mapManufacturer(manu: List<Manufacturer>): List<ManufacturerDto>
    @InheritInverseConfiguration
    fun mapManufacturer(manu: ManufacturerDto): Manufacturer



    fun mapShoppingCartDao(shoppingCart: ShoppingCart): ShoppingCartDto
    fun mapShoppingCartDao (shoppingCarts: List<ShoppingCart>):List<ShoppingCartDto>

    @Mappings(
            Mapping(source = "product.name", target = "name"),
            Mapping(source = "product.description", target = "description"),
            Mapping(source = "quantity", target = "quantity")
    )
    fun mapProductDisplay(selectedProduct: SelectedProduct): DisplayProduct
    fun mapProductDisplay(selectedProduct: List<SelectedProduct>?): List<DisplayProduct>

    @Mappings(
            Mapping(source = "customer.name", target = "customer"),
            Mapping(source = "customer.defaultAddress", target = "address"),
            Mapping(source = "selectedProducts", target = "products")
    )
    fun mapShoppingCartCustomerDao(shoppingCarts: ShoppingCart): ShoppingCustomerDto
    fun mapShoppingCartCustomerDao(shoppingCarts: List<ShoppingCart>): List<ShoppingCustomerDto>



    fun mapSelectedProduct( selectedProduct: SelectedProduct?): SelectedProductDto
    fun mapSelectedProduct( selectedProduct: List<SelectedProduct>?): List<SelectedProductDto>

    fun mapAddress(address: Address): AddressDto

    @InheritInverseConfiguration
    fun mapAddress(address: AddressDto): Address

    @InheritInverseConfiguration
    fun mapProductDto(productDto: ProductDto): Product

    fun mapCustomerDto(customer: Customer?):CustomerDto?
    fun mapCustomerDto (customers: List<Customer>):List<CustomerDto>

    @InheritInverseConfiguration
    fun mapCustomerDto(customerDto: CustomerDto): Customer

    @Mappings(
            Mapping(source = "customer.jwtUser.username", target = "username"),
            Mapping(source = "customer.jwtUser.authorities", target = "authorities")
    )

    fun mapUser(customer : Customer) : UserDto
    fun mapCustomer (customer: Customer) : CustomerDto
    fun mapAuthority(authority: Authority): Authority
    fun mapAuthority (authority : List<Authority>): List<Authority>


}